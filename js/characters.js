/*=================================================
    Файл обеспечивает создание и работу enemy
=================================================*/
//	функция генерирует случайное число
function random(max) {
	// генерируем случайное число от 0 максимум + 1
	var rand = Math.random() * (max+1);
	// округляем число до ближайшего целого
	rand = Math.floor(rand);
	// возвращаем значение rand
	return rand
}
// функция псевдо-случайно возвращает план на котором
// появится enemy (0 - передний, 1 - средний, 2 - задний)
function GetStage() {
	stage = random(2);
	if(stage == 0) {
		return "foreStage";
	} else if(stage == 1) {
		return "middleStage";
	} else {
		return "backStage";
	}

}

// функция в зависимости от значения direction будет задавать направление
// enemy: 0 - left, 1 - right
function GetDirection () {
	// создаем переменную, которая рандомно принимает два значения 0 или 1
	var dir = random(1);
	if(dir == 0){
		direction = "left";
		return "enemy left";
	} 
	direction = "right";
	return "enemy right";
}
// в зависимости от значения переменной characterEnemy будем задавать
// картинку enemy: 0 - Obelix, 1 - Asterix
function GetImage () {
	// создаем переменную, которая рандомно принимает два значения 0 или 1
	var imageEnemy = 0;
	//проверяем, если план передний 
	if(stage == 0) {
		// на передний план выводим только 0 - Obelix, 1 - Asterix
		var imageEnemy = random(1);
		// в зависимости от значения переменной direction
		// задаем направление появления enemy
		switch(direction) {
		case "right":
			if(imageEnemy == 0) {
				return "images/obelixRedRight.gif";
			} else if(imageEnemy == 1) {
				return "images/asterix.gif";
			} 
		case "left":
			if(imageEnemy == 0) {
				return "images/obelixRed.gif";
			} else if(imageEnemy == 1) {
				return "images/asterix.gif";
			} 
	}
	// если план не передний, то 
	} else {
		// на средний и задний планы выводим только 0 - saracin,
		// 1 - saracinCamel, 2 - колесница, 3 - римлянин
		var imageEnemy = random(3);
		// в зависимости от значения переменной direction
		// задаем направление появления enemy
		switch(direction) {
		case "right":
			if(imageEnemy == 0) {
				return "images/saracinRight.gif";
			} else if(imageEnemy == 1) {
				return "images/saracinCamelRight.gif";
			} else if(imageEnemy == 2) {
				return "images/chariotRight.gif"
			} else 
				return "images/romanRightBig.gif";
		case "left":
			if(imageEnemy == 0) {
				return "images/saracinLeft.gif";
			} else if(imageEnemy == 1) {
				return "images/saracinCamelLeft.gif";
			} else if(imageEnemy == 2) {
				return "images/chariotLeft1.gif"
			} else 
				return "images/romanLeftBig.gif";
		}
	}
}


// создаем рандомное число enemy и добавляем их в игровое поле
function CreateEnemy () {
	
	// создаем блок div
	var enemy = document.createElement("div");
	// создаем элемент img
	var image = document.createElement("img");
	// присваиваем атрибуту title значение
	enemy.title = GetStage();
	// присваиваем классу className значение
	enemy.className = GetDirection();
	// присваиваем атрибуту image.src значение
	image.src = GetImage();
	// добавляем элемент image (как дочерний элемент) к созданному тегу div
	enemy.appendChild(image);
	// Добавляем элемент div в игровое поле 
	gameField.appendChild(enemy);
	// слушаем событие клика мышки на enemy и вызываем функцию звука выстрела
	enemy.addEventListener('click', soundOnEnemyClick, false);

	
    // обрабатываем клик мышки на enemy
	enemy.onclick = function() {
		// console.log("Arrows: " + currentValueArrow)
		// проверяем количество стрел и если оно равно нулю, выстрела нет
		if(currentValueArrow == 0) {
			return;
		}
		// данный блок испоняется только в том случае если className enemy
		// не равен enemy wait-delete
		if(enemy.className != "ememy wait-delete") {
			// генерируем случайное число от 0 до 5
			var rand1 = random(5);
			// увеличиваем счетчик очков на полученное случайное число
			starsCounter +=rand1;
		
			// отображаем счет игры, текст берем из переменной starsCounter
			stars.innerText = starsCounter;
			++enemyKilled;
			//console.log(enemyKilled);

			// задаем прозрачность enemy
			enemy.style.opacity = "0.5";
			// удаляем\создаем enemy с определенной задержкой
			setTimeout(function () {
				// удаляем enemy
				enemy.remove();
				// удаляем стрелу
				removeArrow();
				// если стрелы закончились, выводим инструкцию по перезарядке
				if(currentValueArrow == 0) {
					createWindowInstruction();
				}
				// если игра закончена, прекращаем работу функции
				if (gameStatus == "koniec") {
					return;
				}

				// создаем переменную для элемента с классом .enemy
				var existEnemy = document.querySelector(".enemy");
				if(existEnemy == null) {
					// переменная, указывающая сколько enemy нужно создать
					numberEnemies = enemiesNumberDependOnLevel;
					// переменная, указывающая текущее количество enemy
					var currentEnemies = 0;
					//запускаем цикл для создания количества enemy
					// указанных в numberEnemies
					while (currentEnemies < numberEnemies) {
						// создаем enemy
						CreateEnemy();
						// увеличиваем количество текущих enemy на 1
						++currentEnemies;
					} 	// конец цикла	
				}
			}, 20) // конец таймера
		}
		// присваиваем enemy className = "enemy wait-delete"
		enemy.className = "enemy wait-delete";
	};
	
	// запускаем движение enemy и удаляем, если он вышел за
	// пределы игрового поля
	setTimeout(function() {
		// изменяем задержку enemy, заданную в стилях
		enemy.style.transition = "all 0s";
		// создаем таймер, который каждые 10 мл\с двигает enemy
		var timerEnemy = setInterval(function() {
			// проверяем с какой стороны движется enemy
			if(enemy.className == "enemy left") {
				// если слева, то увеличиваем значение left enemy увеличивая текущее 
				// значение left на 2рх
				enemy.style.left = enemy.offsetLeft + speed + "px";
				// проверяем, вышел ли enemy за границы игрового поля
				if(enemy.offsetLeft > 800) {
					// удаляем enemy
					enemy.remove();
					// создаем enemy
					CreateEnemy();
				}	
			} else {
				// если справа, то уменьшаем значение left enemy уменьшая текущее 
				// значение left на 2рх
				enemy.style.left = enemy.offsetLeft - speed + "px";
				// проверяем, вышел ли enemy за границы игрового поля
				if(enemy.offsetLeft < -90) {
					// удаляем enemy
					enemy.remove();
					// создаем enemy
					CreateEnemy();
				}	
			}
		}, 10)
	}, 1000)
} // конец функции