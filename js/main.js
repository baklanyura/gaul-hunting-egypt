// функция стартует при запуске игры
function start() {

	// создаем стартовый блок на игровом поле
	createStartBlock();
	// задаем статус "начать"
	gameStatus = "nachat";
	// при клике на startBttn запускаем функцию gameStart()
	startBttn.onclick = function() {
		soundButtonPress();
		gameStart();
	}
}

// функция удаляет все элементы, выводит блок конца игры, где указаны набранные очки 
// и количество убитых врагов, обнуляет счетчик очков. 
function gameOver () {
	// останавливаем музыку
	 mainTheme.pause();
	// удаляем все элементы 
	removeAllElements();
	// создаем блок окончания игры
	createEndBlock();
	// обнуляем счетчик очков
	starsCounter = 0;
	// задаем статус "конец" для окончания игры
	gameStatus = "koniec";
	// останавливаем таймер
	clearInterval(StartTimer);
}

// функция удаляет все элементы, вызывает функции CreateArrows()
//  createEnemy() и createStars(), которые выводят на игровое поле 
// элементы: enemy, очки и стрелы. А также вызываем функции CreateTimerBlock
// и StartTimer, которые  запускает обратный отсчет, а также запускаем музыку.
function gameStart () {
	// startBlock.style.display = "none";
	removeAllElements();
	// при нажатии на кнопку запускаем музыку стартовой страницы
	soundMainThemeStartPagePause();
	// при старте игры запускаем музыку
	soundMainTheme();
	//создаем enemy
	CreateEnemy();
	// создаем очки
	createStars();
	// добавляем muteBttn
	AppendMuteButton();
	// создаем таймер	
	CreateTimerBlock();
	// запускаем таймер	
	StartTimer();
	// создаем стрелы
	CreateArrows();

}
// создаем игру
start();
