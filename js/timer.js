/*=================================================
   Файл обеспечивает создание и работу таймера
=================================================*/
function CreateTimerBlock() {
	// помещаем в переменную timerBlock элемент <h3>
	var timerBlock = document.createElement("h3");
	// помещаем в переменную myTimer ссылку на span "my-timer"
	myTimer = document.createElement("span");
	
	myTimer.id = "my-timer";
	// присваиваем переменной текстовое значение span
	myTimer.innerText = "01:40";


	timerBlock.appendChild(myTimer);
	gameField.appendChild(timerBlock);
}
// функция увеличивает количество создаваемых врагов на 1
function IncreaseNumberEnemies() {
	enemiesNumberDependOnLevel +=1;
}
// переменная для вычисления 10 секундного промежутка
var i = 90;
function StartTimer() {
	
	var time = myTimer.innerHTML;
	// метод split - делит строку на массив подстрок, в кавычках указан 
	// сепаратор, по которой делится строка. При разбиении на подстроки
	// сепаратор проподает
	var arr = time.split(":");
	// первое значение массива arr содержит минуты, что и помещаем
	// в переменную min  
	var min = arr[0];
	// второе значение массива arr содержит секунды, и их также помещаем
	// в переменную sec
	var sec = arr[1];
	
	// проверяем не вышло ли время
	  if (sec == 0) {
	    if (min == 0) {
	        // если секунды и минуты равны 0, то завершаем игру
	        console.log("gameover");

	        // останавливаем таймер
			clearInterval(StartTimer);
	  		// завершаем игру
			gameOver();
	      // если минуты равны нулю   
	      min = 60;
	    
	    }
	    // уменьшаем минуты на единицу
	    min--;
	    // если переменная min меньше 10, то минуты выводим в формате 09, 08 и т.д.
	    if (min < 10) {
	    	// выводим значение минут с добавлением нуля
	    	min = "0" + min;
	    }
	    // если секунды равны 0
	    sec = 59;
	  }
	  // если время не вышло уменьшаем секунды на 1
	  else sec--;
	  // уменьшаем значение переменной timerCounter на 1	 
	  timerCounter--;
	  // если разница двух переменных равна нулю занчит прошло 10 секунд
	  if((timerCounter - i) == 0) {
	  	// увеличиваем количестов создаваемых врагов на 1
	  	IncreaseNumberEnemies();
	  	// уменьшаем значение переменной i на 1
	  	i -= 10;
	  }
	  // если переменная sec меньше 10, то секунды выводим в формате 
	  // 09, 08 и т.д., а также изменяем цвет таймера и запускаем звук
	  if (sec < 10) {
	    // выводим значение секунд с добавлением нуля
	    sec = "0" + sec;
	  }
	  // если до конца игры осталось 10 секунд
	  if(min == 0 && sec < 10) {
	  	// меняем цвет таймера
	  	myTimer.style.color = "red";
	  	// запускаем звуковой сигнал
	    soundBeep();
	  }
	  // выводим значение переменных min и sec в span
	  myTimer.innerHTML = min+":"+sec;
	  // запускаем задержку в 1 секунду
	  setTimeout(StartTimer, 1000);
	}