/*=================================================
  Файл обеспечивает создание и работу блоков игры
=================================================*/

// создаем стартовый блок и добавляем его на игровое поле
function createStartBlock() {
	//создаем блок див <div id="start-block">
	startBlock = document.createElement("div");
	// присваиваем знкчение id переменной startBlock
	startBlock.id = "start-block";
	// создаем кнопку <button id="start-bttn">Начать</button>
	startBttn = document.createElement("button");
	// присваиваем знкчение id переменной startBttn
	startBttn.id = "start-bttn";
	// задаем текст переменной startBttn
	startBttn.innerText = "Start game";
	// добавляем кнопку в стартовый блок
	startBlock.appendChild(startBttn);
	// добавляем кнопки выбора уровня в стартовый блок
	startBlock.appendChild(CreateLevelButtons());
	// добавляем текст названия игры в стартовый блок
	startBlock.appendChild(CreateGameTitle());
	// добавляем ходящего египтянина в стартовый блок
	startBlock.appendChild(CreateEgyptian());
	AppendMuteButtonStartPage();
	// добавляем стартовый блок на игровое поле
	gameField.appendChild(startBlock);
	// запускаем музыку стартовой страницы
	soundMainThemeStartPage();
}
function CreateGameTitle() {
	// создаем переменную для элемента div
	var gameTitle = document.createElement("div");
	// присваиваем знкчение id переменной gameTitleText
	gameTitle.id = "title-high-score";
	// создаем переменную для элемента h1
	var gameTitleText = document.createElement("h1");
	// присваиваем знкчение id переменной gameTitleText
	gameTitleText.id = "title-text";
	// присваиваем строковое значение h1
	gameTitleText.innerText = "Gaul Hunting Egypt";
	// добавляем заголовок h1 в элемент div
	gameTitle.appendChild(gameTitleText);
	// функция возвращает переменную gameTitle
	return gameTitle;
}



function CreateLevelButtons() {
	// создаем переменную для элемента div
	var containerLevels = document.createElement("div");
	//присваеваем блоку id="container-levels"
	containerLevels.id = "container-levels";

    /// создаём кнопку уровня игры easy level
    var buttonEasyLevel = document.createElement("button");
	//записываем текст в созданную кнопку
	buttonEasyLevel.innerText = "Easy";
    //присваиваем кнопке id="easy-level-btn"
    buttonEasyLevel.id = "easy-level-btn";
    // при нажатии на кнопку изменяем скорость передвижения enemy 
    // и запускаем звук нажатия кнопки
    buttonEasyLevel.onclick = function () {
    	// устанавливаем скорость передвижения enemy на игровом поле
    	// для данного уровня
    	speed = 1;
    	// изменяем цвет фона конпки
    	buttonEasyLevel.style.background = "red";
    	// изменяем цвет фона конпки
		buttonMiddleLevel.style.background = "#008B8B";
		// изменяем цвет фона конпки
		buttonHardLevel.style.background = "#008B8B"
		// запускаем звук нажатия кнопки
    	soundButtonPress();
    }
	    
    // создаём кнопку уровня игры middle level button
     var buttonMiddleLevel = document.createElement("button");
	//записываем текст в созданную кнопку
	buttonMiddleLevel.innerText = "Middle";
    //присваиваем кнопке id="middle-level-btn"
    buttonMiddleLevel.id = "middle-level-btn";
    // при нажатии на кнопку изменяем скорость передвижения enemy
    // и запускаем звук нажатия кнопки
    buttonMiddleLevel.onclick = function () {
    	// устанавливаем скорость передвижения enemy на игровом поле
    	// для данного уровня
    	speed = 2;
    	// изменяем цвет фона конпки
    	buttonEasyLevel.style.background = "#008B8B";
    	// изменяем цвет фона конпки
		buttonMiddleLevel.style.background = "red";
		// изменяем цвет фона конпки
		buttonHardLevel.style.background = "#008B8B"
		// запускаем звук нажатия кнопки
    	soundButtonPress();
    }
    // создаём кнопку уровня игры hard level button
    var buttonHardLevel = document.createElement("button");
	//записываем текст в созданную кнопку
	buttonHardLevel.innerText = "Hard";     
    //присваиваем кнопке id="hard-level-btn"
    buttonHardLevel.id = "hard-level-btn";
    // при нажатии на кнопку изменяем скорость передвижения enemy
    // и запускаем звук нажатия кнопки
    buttonHardLevel.onclick = function () {
    	// устанавливаем скорость передвижения enemy на игровом поле
    	// для данного уровня
    	speed = 4;
    	// изменяем цвет фона конпки
    	buttonEasyLevel.style.background = "#008B8B";
    	// изменяем цвет фона конпки
		buttonMiddleLevel.style.background = "#008B8B";
		// изменяем цвет фона конпки
		buttonHardLevel.style.background = "red";
		// запускаем звук нажатия кнопки
    	soundButtonPress();
    }
    //добавляем кнопки уровней игры в блок с уровнями
    containerLevels.appendChild(buttonEasyLevel);
    containerLevels.appendChild(buttonMiddleLevel);
    containerLevels.appendChild(buttonHardLevel);
    // функция возвращает переменную containerLevels
    return containerLevels;
}

// создаем элемент очки и добавляем его в игровое поле
function createStars () {
	// создаем переменную для элемента div
	stars = document.createElement("div");
	// добовляем тегу div => id="stars"
	stars.id = "stars";
	// вводим текст который будет отображаться на экране
	stars.innerText = "0";
	// Добавляем элемент очки в игровое поле <div id="igra"></div>
	gameField.appendChild(stars);
}

// добавляем кнопку muteBttn на игровое поле и слушаем событие click
function AppendMuteButtonStartPage() {
	//добавляем muteBttn на игровое поле. Кнопка создана в файле sounds
	startBlock.appendChild(CreateMuteButtonStartPage());
	// слушаем событие нажатия кнопки muteBttn
 	muteBttnStartPage.addEventListener('click', MuteStart, false);
}

// добавляем кнопку muteBttn на игровое поле и слушаем событие click
function AppendMuteButton() {
	//добавляем muteBttn на игровое поле. Кнопка создана в файле sounds
	gameField.appendChild(CreateMuteButton());
	// слушаем событие нажатия кнопки muteBttn
 	muteBttn.addEventListener('click', Mute, false);
}
// создаем ходящего египтянина
function CreateEgyptian() {
	// создаем переменную для элемента div
	var egyptian = document.createElement("div");
	// создаем переменную для элемента img
	var img = document.createElement("img");
	// добавляем тегу div => id="egyptian"
	egyptian.id = "egyptian";
	// задаем путь до картинки для переменной img
	img.src = "images/egyptianRight.gif";
	// добавляем в div блок картинку
	egyptian.appendChild(img);
	// функция возвращает переменную egyptian
	return egyptian;
}

function CreateArrows() {
	//создаём блок для стрел <div id="arrows-block">
	 arrowsBlock = document.createElement("div");
	//присваиваем блоку id="arrows-block"
	arrowsBlock.id = "arrows-block";
	// исходное количество стрел
	var numberArrow = 7;
	//переменная содержит текущее количество стрел
	 currentValueArrow = 0;
	// запускаем цикл с условием пока текущее количество стрел
	// меньше исходного количества
	while(currentValueArrow < numberArrow) {
		 // statusBlockArrows = "empty";
		//создаем спан
		unitArrow = document.createElement("span");
		// присваиваем спану id
		unitArrow.id = "unit-arrow";
		// спан добавляется в блок стрел
		arrowsBlock.appendChild(unitArrow);
		// количество текущих стрел увеличивается на +1
		currentValueArrow = currentValueArrow + 1;
	}
	// добавляем блок стрел в игровое поле
	 gameField.appendChild(arrowsBlock);
    
}
// отслеживает нажатие правой кнопкой мышки на игровом поле
gameField.oncontextmenu = function(event) {

	event.preventDefault();
	//если кол-во стрел = 0 то выполняется ф-ция
	if(currentValueArrow == 0) {
	    // добавляем звук при перезарядки 
	   soundReload();
	   // убираем подсказку
	   removeInstruction();
	   //создаем стрелы
	    setTimeout(CreateArrows(), 2000);
	    console.log("Current arrows: " + currentValueArrow);
	// если стрелы еще есть то не реагируем на нажатие
	} else {
		return;
	}
}	

//создаем окошко с текстом пояснения как сделать перезарядку
function createWindowInstruction() {
	//создаём тег <span>
	instruction = document.createElement("span");
	//присваиваем спану id 	<span id="instruction">
	instruction.id = "instruction";
	//добавляем в <span id="instruction"> текст
	instruction.innerText = "CLICK RIGHTBUTTON TO RELOAD";
		// Добавляем созданный <span id="instruction"> с текстом в игровое поле <div id="igra"></div>
	gameField.appendChild(instruction);
}

// создаем блок окончания игры
function createEndBlock() {
	//запускаем музыку
	mainThemeStartPage.play();
	// создаем див элемент <div id="end-game"></div>
	endBlock = document.createElement("div");
		// присваиваем id id="end-game"
		endBlock.id = "end-game";
	// создаем элемент h1 для див
	var h1 = document.createElement("h1");
		// задаем текст
		h1.innerText = "Game Over";
	// создаем элемент h2 для див	
	var eKilled = document.createElement("h3");
		eKilled.innerText = "You killed: " + enemyKilled + " enemies";
	// создаем элемент h2 для див
	var h2 = document.createElement("h2");
		// задаем текстовое значение с указанием набранных очков
		h2.innerText = "You scored: " + starsCounter + " points";
	// создаем элемент button для див
	var bttn = document.createElement("button");
		// присваиваем значение для id
		bttn.id = "bttn";
		// задаем текстовое значение
		bttn.innerText = "Play again";
		

	// добовляем элемент h1 в div
	endBlock.appendChild(h1);
	// добовляем элемент h3 в div
	endBlock.appendChild(eKilled);
	// добовляем элемент h3 в div
	endBlock.appendChild(h2);
	// добовляем элемент button в div
	endBlock.appendChild(bttn);
	// добавляем див на игровое поле
	gameField.appendChild(endBlock);
	// при клике на кнопку вызываем функцию soundButtonPress()
	// и перезагружаем страницу
    bttn.onclick = function () {
    	soundButtonPress();
    	ReloadGame();
    }
}
// функция перезагружает страницу
function ReloadGame() {
	window.location.reload();
}

/*==============================
Функции для удаления блоков игры
================================*/

// функция удаляет все элементы на игровом поле
// startBlock
function removeAllElements() {
	// запускаем цикл, который проверяет есть ли дочерние
	// элементы у элемента gameField
	while (gameField.children.length>0) {
		// удаляем последний дочерний элемент
		gameField.removeChild(gameField.lastChild);
	}
}

// функция удаляет элемент startBlock на игровом поле
function removeStartBlock() {
	// удалить startBlock
	startBlock.remove();
}

// функция удаляет элемент stars на игровом поле
function removeStars() {
	// удалить stars
	stars.remove();
}

// функция удаляет элемент endBlock на игровом поле
function removeEndBlock() {
	// удалить endBlock
	endBlock.remove();
}

// функция удаляет элемент timerBlock на игровом поле
function removeTimerBlock() {
	// удалить timerBlock
	timerBlock.remove();
}

//Удаление 1-го <span> со стрелой из блока стрел
function removeArrow() {
	//передаём в переменную элемент <span id="unit-arrow">
	unitArrow = document.getElementById("unit-arrow");
	//Удаляем <span id="unit-arrow">
	unitArrow.remove();
	currentValueArrow--;
}

function removeInstruction() {
	instruction.remove();
}
