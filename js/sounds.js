/*=================================================
     Файл обеспечивает создание и работу audio
=================================================*/
// создаем кнопку muteBttnStartPage
function CreateMuteButtonStartPage() {
	// создаем элемент button
	muteBttnStartPage = document.createElement("button");
	// присваиваем id = "muteStart-bttn" для переменной
	muteBttnStartPage.id = "muteStart-bttn";
	// указывает путь до картинки
	muteBttnStartPage.style.backgroundImage = 'url(images/nota.jpg)';
	// функция возвращает переменную muteBttnStartPage
	return muteBttnStartPage;

}
// создаем кнопку muteBttn
function CreateMuteButton() {
	// создаем элемент button
	muteBttn = document.createElement("button");
	// присваиваем id = "muteStart-bttn" для переменной
	muteBttn.id = "mute-bttn";
	// указывает путь до картинки
	muteBttn.style.backgroundImage = 'url(images/gameFieldNota.png)';
	// функция возвращает переменную muteBttn
	return muteBttn;
}

//функция загрузки Audio для музыкальной темы стартовой страницы
 function soundMainThemeStartPage() {
	// указываем путь до аудио файла основной музыкальной 
	// темы игры
	mainThemeStartPage.src = 'audio/walkLikeAnEgyptian.mp3';
	// воспроизводим аудио
	mainThemeStartPage.mute = "true";
 }
 // функция ставит на паузу музыкальную тему стартовой страницы
 function soundMainThemeStartPagePause() {
 	mainThemeStartPage.pause();
 }

//функция загрузки основной музыкальной темы игры
 function soundMainTheme() {
	// указываем путь до аудио файла основной музыкальной 
	// темы игры
	mainTheme.src = 'audio/mainTheme.mp3';
	// воспроизводим аудио
	mainTheme.play();
 }
 // функция ставит на паузу основрую музыкальную тему игры
 function soundMainThemePause() {
 	mainTheme.pause();
 }
// функция вкл/выкл выключает основную музыкалльную тему стартовой страницы
function MuteStart () {
	console.log("clicked");
	// проверяем состояние флажка
	if(mainThemeMuteStartPage == "Mute") {
		// ставим на паузу
		soundMainThemeStartPagePause();
		// изменяем состояние флажка на противоположное
		mainThemeMuteStartPage = "Play";
		// изменяем путь до картинки
		muteBttnStartPage.style.backgroundImage = 'url(images/notaMute.jpg)';
		// выходим из функции
		return;
	} 
	// запускаем музыку
	mainThemeStartPage.play();
	// изменяем состояние флажка на противоположное
	mainThemeMuteStartPage = "Mute";
	// изменяем путь до картинки
	muteBttnStartPage.style.backgroundImage = 'url(images/nota.jpg)';
}

// функция вкл/выкл выключает основную музыкалльную тему игры
function Mute () {
	// проверяем состояние флажка
	if(mainThemeMute == "Mute") {
		// ставим на паузу
		soundMainThemePause();
		// изменяем состояние флажка на противоположное
		mainThemeMute = "Play";
		// изменяем путь до картинки
		muteBttn.style.backgroundImage = 'url(images/gameFieldNotaMute.png)';
		// выходим из функции
		return;
	} 
	// запускаем музыку
	mainTheme.play();
	// изменяем состояние флажка на противоположное
	mainThemeMute = "Mute";
	// изменяем путь до картинки
	muteBttn.style.backgroundImage = 'url(images/gameFieldNota.png)';
}
//функция звука выстрела
function soundArrow() {
	//создаем объект Audio для выстрела
	var arrow = new Audio ();
	// указываем путь до аудио файла выстрела
	arrow.src = "audio/arrow.mp3";
	// воспроизводим аудио
 	arrow.play();
}
//функция звукового сигнала окончания таймера
function soundBeep() {
	//создаем объект Audio
	var audio = new Audio();
	// указываем путь до аудио файла
	audio.src = 'audio/beep.mp3';
	audio.play();
}

//функция звука нажатия кнопки
function soundButtonPress() {
	//создаем объект Audio
	var audio = new Audio();
	// указываем путь до аудио файла
	audio.src = 'audio/buttonPress.mp3';
	audio.play();
}

//функция звука перезарядки
function soundReload() {
	//создаем объект Audio
	var audio = new Audio();
	// указываем путь до аудио файла
	audio.src = 'audio/reload.mp3';
	// воспроизводим аудио
	audio.play();	
}

function soundOsechka() {
	//создаем объект Audio
	var audio = new Audio();
	// указываем путь до аудио файла
	audio.src = 'audio/osechka.mp3';
	audio.play();
}

function soundOnEnemyClick() {
	if(currentValueArrow == 0) {
		soundOsechka();
		return;
	}
	soundArrow();
	setTimeout(function() {
	//создаем объект Audio
	var audio = new Audio();
	// указываем путь до аудио файла
	audio.src = 'audio/scream2.mp3';
	audio.play();
}, 200);
}