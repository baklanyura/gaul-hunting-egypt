// stars - очки на игровом поле
var stars = null;
// gameField - блок игровое поле
var gameField = document.querySelector("#igra");
// startBlock - блок для стартовой страницы
var startBlock = null;
// startBttn - кнопка для запуска игры на стартовой странице
var startBttn = null;
// muteBttn - кнопка вкл/выкл музыки
var muteBttn = null;
// блок окончания игры
var endBlock = null;
//создаем объект Audio для музыкальной темы стартовой страницы
var mainThemeStartPage = new Audio();
//создаем объект Audio для основной музыкальной темы игры
var mainTheme = new Audio();
// переменная флажок для определения состояния 
// вкл/выкл музыкальной темы стартовой страницы
var mainThemeMuteStartPage = "Play";
// переменная флажок для определения состояния 
// вкл/выкл основной музыкальной темы игры
var mainThemeMute = "Mute";
// создаем переменную для установки скорости
// передвижения enemy на игровом поле
// создаем переменную для инструкции по перезалядке
var instruction = null;
// создаем переменную для скорости движения enemy
var speed = 1;
// создаем переменную для выбора 
var stage = 0;  
// создаем переменную для выбора направления (слева, справа)
var direction = null;
// счетчик количества убитых врагов.
var enemyKilled = 0;
//myTimer - блок для обратного отсчета
var myTimer = null;
// создаем переменную для количества создаваемых enemy
var enemiesNumberDependOnLevel = 2;
// создаем переменную для вычисления 10 секундного интервала
// начальное значение равно количеству секунд игрового времени
var timerCounter = 100;
// starsCounter - переменная для изменения очков на игровом поле
var starsCounter = 0;
//переменная span со стрелой
var unitArrow = null; 
// создаем переменную для текущего количества стрел
var currentValueArrow = 0;
// переменная содержит текущий статус игры
var gameStatus = "neutral";